package com.hcid.intern.db.controller;



import com.hcid.intern.db.model.TokenData;
import com.hcid.intern.db.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/api/v1/contract")
public class TokenController {

    @Autowired
    private TokenService tokenService;

    @PostMapping("/save-token")
    public ResponseEntity<?> saveToken(@RequestBody TokenData tokenData){

        // do validation

        TokenData tokenData1 = new TokenData();
        tokenData1.setName("jonny kemot");
        tokenData1.setContractNumber("24234234");

        // save contract data
        tokenService.saveToken(tokenData);

        return new ResponseEntity(tokenData, HttpStatus.OK);
    }

    @PostMapping("/update-token")
    public ResponseEntity<?> updateToken(@RequestBody TokenData tokenData){

        // do validation

        // save contract data
        tokenService.updateToken(tokenData);

        return new ResponseEntity(tokenData, HttpStatus.OK);
    }

    @GetMapping("/delete-Token/{tokenId}")
    public ResponseEntity<?> deleteToken(@PathVariable String tokenId){

        // do validation

        // save contract data
        tokenService.deleteToken(tokenId);

        return new ResponseEntity(tokenId, HttpStatus.OK);
    }
}
