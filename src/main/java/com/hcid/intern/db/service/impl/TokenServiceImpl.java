package com.hcid.intern.db.service.impl;


import com.hcid.intern.db.model.TokenData;
import com.hcid.intern.db.repository.TokenRepository;
import com.hcid.intern.db.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokenServiceImpl implements TokenService {

    @Autowired
    private TokenRepository tokenRepository;



    public void saveToken(TokenData tokenData){
        tokenRepository.save(tokenData);
    }

    @Override
    public void updateToken(TokenData tokenData) {
        tokenRepository.save(tokenData);
    }

    @Override
    public void deleteToken(String contractId) {
        tokenRepository.delete(contractId);
    }


}
