package com.hcid.intern.db.service;


import com.hcid.intern.db.model.TokenData;

public interface TokenService {

    void saveToken(TokenData tokenData);

    void updateToken(TokenData tokenData);

    void deleteToken(String contractId);
}
